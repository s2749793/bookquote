package nl.utwente.di.bookQuote;

import java.util.Map;

public class Quoter {
    public static final Map<String,Double> PRICES = Map.ofEntries(
        Map.entry("1",10d),
        Map.entry("2",45d),
        Map.entry("3",20d),
        Map.entry("4",35d),
        Map.entry("5",50d)
    );

    public double getBookPrice(String isbn) {
        return PRICES.getOrDefault(isbn,0d);
    }
}
